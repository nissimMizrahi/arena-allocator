#include "SimpleAllocator.h"



SimpleAllocator::SimpleAllocator(size_t size, size_t expected_free_bubbles)
{
	freeBubbles.reserve(expected_free_bubbles);
	arena = malloc(size);

	freeBubbles.emplace_back(Bubble{
		arena,
		size
	});
}

SimpleAllocator::SimpleAllocator(void* buffer, size_t size, size_t expected_free_bubbles)
{
	freeBubbles.reserve(expected_free_bubbles);
	arena = buffer;

	freeBubbles.emplace_back(Bubble{
		arena,
		size
	});
}

SimpleAllocator::~SimpleAllocator()
{
	free(arena);
}